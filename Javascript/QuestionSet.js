var questionIndex;

function questionset() {
    //variable for the file name that we're looking at
    var filename = location.pathname.split('/').slice(-1).toString();

    //switch to determine which set of questions we draw from
    switch(filename)
    {
        case "Reading%20Algebraic%20Notation%20Quiz%20I.html":
            var askedquestions = [
                {question: 'The image below contains a symbol commonly used in statistics. Which is the most appropriate definition?', image: '../../Images/mu.png', answer1: 'The population parameter of interest', answer2: 'The sample statistic of interest', answer3: 'The proportion of an occurrence that we are looking for in the dataset of a population', answer4: 'The proportion of an occurrence that we are looking for in the dataset of a sample', correct: null, type: 'multiple choice', 
                explanation: ['The symbol presented is simply a symbol to represent a piece of statistics language.', '* The mu symbol (the one in our question) represents the population parameter of interest.', '* The x-bar symbol represents the sample statistic of interest.', '* The p symbol represents the population proportion of interest.', '* The p-hat symbol represents the sample proportion of interest.']},
                {question: 'The image below contains a symbol commonly used in statistics. Which is the most appropriate definition?', image: '../../Images/xbar.png', answer1: 'The sample statistic of interest', answer2: 'The population parameter of interest', answer3: 'The proportion of an occurrence that we are looking for in the dataset of a population', answer4: 'The proportion of an occurrence that we are looking for in the dataset of a sample', correct: null, type: 'multiple choice',
                explanation: ['The symbol presented is simply a symbol to represent a piece of statistics language.', '* The mu symbol represents the population parameter of interest.', '* The x-bar (the one in our question) symbol represents the sample statistic of interest.', '* The p symbol represents the population proportion of interest.', '* The p-hat symbol represents the sample proportion of interest.']},
                {question: 'The image below contains a symbol commonly used in statistics. Which is the most appropriate definition?', image: '../../Images/p.png', answer1: 'The proportion of an occurrence that we are looking for in the dataset of a population', answer2: 'The sample statistic of interest', answer3: 'The population parameter of interest', answer4: 'The proportion of an occurrence that we are looking for in the dataset of a sample', correct: null, type: 'multiple choice',
                explanation: ['The symbol presented is simply a symbol to represent a piece of statistics language.', '* The mu symbol represents the population parameter of interest.', '* The x-bar symbol represents the sample statistic of interest.', '* The p symbol (the one in our question) represents the population proportion of interest.', '* The p-hat symbol represents the sample proportion of interest.']},
                {question: 'The image below contains a symbol commonly used in statistics. Which is the most appropriate definition?', image: '../../Images/p-hat.png', answer1: 'The proportion of an occurrence that we are looking for in the dataset of a sample', answer2: 'The sample statistic of interest', answer3: 'The proportion of an occurrence that we are looking for in the dataset of a population', answer4: 'The population parameter of interest', correct: null, type: 'multiple choice',
                explanation: ['The symbol presented is simply a symbol to represent a piece of statistics language.', '* The mu symbol represents the population parameter of interest.', '* The x-bar symbol represents the sample statistic of interest.', '* The p symbol represents the population proportion of interest.', '* The p-hat (the one in our question) symbol represents the sample proportion of interest.']},
                {question: 'Our parameter of interest (\u03BC) is the average stem length. What is the value of our parameter? (don\'t forget to include the units in your answer)', image: '../../Images/pop-sam1.png', answer1: null, answer2: null, answer3: null, answer4: null, correct: '8cm', type: 'input',
                explanation: ['Firstly, what is the question asking us? It\'s asking us what the value of our parameter is', '* A parameter is information that relates to a population', '* A statistic is information that related to a sample', 'As we are being asked about the parameter, we use the population information - we\'re intereseted in the average of which is (5cm + 3cm + 10cm + 9cm + 13cm)/5', 'This is 40cm/5 which is 8cm in length', 'Don\'t forget to include the units - in our case cm']},
                {question: 'Our parameter of interest (\u03BC) is the average movement speed of a person. What is the value of our parameter? (don\'t forget to include the units in your answer)', image: '../../Images/pop-sam2.png', answer1: null, answer2: null, answer3: null, answer4: null, correct: '6km/h', type: 'input',
                explanation: ['Firstly, what is the question asking us? It\'s asking us what the value of our parameter is', '* A parameter is information that relates to a population', '* A statistic is information that related to a sample', 'As we are being asked about the parameter, we use the population information - we\'re intereseted in the average of which is (5km/h + 8km/h + 10km/h + 7km/h + 4km/h + 7km/h + 1km/h)/7', 'This is (42km/h)/7 which is 6km/h in length', 'Don\'t forget to include the units - in our case km/h']},
                {question: 'Our parameter of interest (\u03BC) is the average time and animal spends on a cellphone. What is the value of our parameter? (don\'t forget to include the units in your answer)', image: '../../Images/pop-sam3.png', answer1: null, answer2: null, answer3: null, answer4: null, correct: '5mins', type: 'input',
                explanation: ['Firstly, what is the question asking us? It\'s asking us what the value of our parameter is', '* A parameter is information that relates to a population', '* A statistic is information that related to a sample', 'As we are being asked about the parameter, we use the population information - we\'re intereseted in the average of which is (3mins + 5mins + 7mins)/3', 'This is 15mins/3 which is 5mins in length', 'Don\'t forget to include the units - in our case mins']},
                {question: 'Our statistic of interest (x-bar) is the average stem length. What is the value of our statistic? (don\'t forget to include the units in your answers)', image: '../../Images/pop-sam1.png', answer1: null, answer2: null, answer3: null, answer4: null, correct: '7cm', type: 'input',
                explanation: ['Firstly, what is the question asking us? It\'s asking us what the value of our statistic is', '* A parameter is information that relates to a population', '* A statistic is information that related to a sample', 'As we are being asked about the statistic, we use the sample information - we\'re intereseted in the average of which is (5cm + 3cm + 13cm)/3', 'This is 21cm/3 which is 7cm in length', 'Don\'t forget to include the units - in our case cm']},
                {question: 'Our parameter of interest (x-bar) is the average time and animal spends on a cellphone. What is the value of our statistic? (don\'t forget to include the units in your answer)', image: '../../Images/pop-sam2.png', answer1: null, answer2: null, answer3: null, answer4: null, correct: '5km/h', type: 'input',
                explanation: ['Firstly, what is the question asking us? It\'s asking us what the value of our statistic is', '* A parameter is information that relates to a population', '* A statistic is information that related to a sample', 'As we are being asked about the statistic, we use the sample information - we\'re intereseted in the average of which is (5km/h + 7km/h + 7km/h + 1km/h)/4', 'This is 21cm/3 which is 5km/h in length', 'Don\'t forget to include the units - in our case km/h']},
                {question: 'Our parameter of interest (x-bar) is the average time and animal spends on a cellphone. What is the value of our statistic? (don\'t forget to include the units in your answer)', image: '../../Images/pop-sam3.png', answer1: null, answer2: null, answer3: null, answer4: null, correct: '4mins', type: 'input',
                explanation: ['Firstly, what is the question asking us? It\'s asking us what the value of our statistic is', '* A parameter is information that relates to a population', '* A statistic is information that related to a sample', 'As we are being asked about the statistic, we use the sample information - we\'re intereseted in the average of which is (3mins + 5mins)/2', 'This is 8mins/2 which is 4mins in length', 'Don\'t forget to include the units - in our case mins']},
                {question: 'What is the (p) for animals over 0.5 meters tall? (round to the nearest hundredth)', image: '../../Images/p-phat1.png', answer1: null, answer2: null, answer3: null, answer4: null, correct: '0.67', type: 'input',
                explanation: ['Well, what are we looking for? If our symbol is (p) than we\'re looking to the population information, if our symbol is (p-hat) than we\'re looking to the sample information', 'As our symbol is (p) so we look to the population information, and we\'re looking for the population members who\'s height is over 0.5 meters tall', 'A count of the population members who\'s height is greater than 0.5 meters shows 2 members', 'The total population has 3 members.', 'Therefore the proportion of the population that is over 0.5 meters is 2/3 or 0.67']},
                {question: 'What is the (p) for stars that have a heat of 8,000 K or greater? (round to the nearest hundredth)', image: '../../Images/p-phat2.png', answer1: null, answer2: null, answer3: null, answer4: null, correct: '0.4', type: 'input',
                explanation: ['Well, what are we looking for? If our symbol is (p) than we\'re looking to the population information, if our symbol is (p-hat) than we\'re looking to the sample information', 'As our symbol is (p) so we look to the population information, and we\'re looking for the population members who\'s temperture is over 8000K', 'A count of the population members who\'s temperture is over 8000K shows 2 members', 'The total population has 5 members.', 'Therefore the proportion of the population that is over 8000K  is 2/5 or 0.4']},
                {question: 'What is the (p) for the number of people who have a favourite number of 7? (round to the nearest hundredth)', image: '../../Images/p-phat3.png', answer1: null, answer2: null, answer3: null, answer4: null, correct: '0.29', type: 'input',
                explanation: ['Well, what are we looking for? If our symbol is (p) than we\'re looking to the population information, if our symbol is (p-hat) than we\'re looking to the sample information', 'As our symbol is (p) so we look to the population information, and we\'re looking for the population members who\'s favourite number is 7', 'A count of the population members who\'s favourite number is 7 shows 2 members', 'The total population has 7 members.', 'Therefore the proportion of the population that have a favourite number 7 is 2 members 2/7 or 0.29']},
                {question: 'What is the (p-hat) for animals over 0.5 meters tall? (round to the nearest hundredth)', image: '../../Images/p-phat1.png', answer1: null, answer2: null, answer3: null, answer4: null, correct: '1', type: 'input',
                explanation: ['Well, what are we looking for? If our symbol is (p) than we\'re looking to the population information, if our symbol is (p-hat) than we\'re looking to the sample information', 'As our symbol is (p-hat) so we look to the sample information, and we\'re looking for the sample members who\'s height is over 0.5 meters tall', 'A count of the sample members who\'s height is greater than 0.5 meters shows 1 members', 'The total sample has 2 members.', 'Therefore the proportion of the sample that is over 0.5 meters is 1/2 or 0.5']},
                {question: 'What is the (p-hat) for stars that have a heat of 8,000 K or greater? (round to the nearest hundredth)', image: '../../Images/p-phat2.png', answer1: null, answer2: null, answer3: null, answer4: null, correct: '0.5', type: 'input',
                explanation: ['Well, what are we looking for? If our symbol is (p) than we\'re looking to the population information, if our symbol is (p-hat) than we\'re looking to the sample information', 'As our symbol is (p-hat) so we look to the sample information, and we\'re looking for the sample members who\'s temperture is over 8000K', 'A count of the sample members who\'s temperture is over 8000K shows 2 members', 'The total sample has 4 members.', 'Therefore the proportion of the sample that is over 8000K  is 2/4 or 0.5']},
                {question: 'What is the (p-hat) for the number of people who have a favourite number of 7? (round to the nearest hundredth)', image: '../../Images/p-phat3.png', answer1: null, answer2: null, answer3: null, answer4: null, correct: '0', type: 'input',
                explanation: ['Well, what are we looking for? If our symbol is (p) than we\'re looking to the population information, if our symbol is (p-hat) than we\'re looking to the sample information', 'As our symbol is (p-hat) so we look to the sample information, and we\'re looking for the sample members who\'s favourite number is 7', 'A count of the sample members who\'s favourite number is 7 shows 0 members', 'The total sample has 2 members.', 'Therefore the proportion of the sample that have a favourite number 7 is 0 members 0/2 or 0']},
            ];
            break;
        case "Reading%20Algebraic%20Notation%20Quiz%20II.html":
            var askedquestions = [
                {question: 'The image below contains a symbol commonly used in statistics. Which is the most appropriate definition?', image: '../../Images/sigma.png', answer1: 'It represents the spread that we see in a population', answer2: 'It represents the spread that we see in a sample', answer3: 'It represents the number of units in a population or sample', answer4: 'It represents the shorthand for summing a lot of information together', correct: null, type: 'multiple choice',
                explanation: ['The symbol presented is simply a symbol to represent a piece of statistics language.', '* The sigma symbol (the one in our question) represents the spread of the population.', '* The s symbol represents the spread of the sample.', '* The n symbol represents the number of units in the population or in the sample', '* The (sum-of) symbol represents a short-hand for the addition of a set of information']},
                {question: 'The image below contains a symbol commonly used in statistics. Which is the most appropriate definition?', image: '../../Images/s.png', answer1: 'It represents the spread that we see in a sample', answer2: 'It represents the spread tha we see in a population', answer3: 'It represents the number of units in a population or sample', answer4: 'It represents the shorthand for summing a lot of information together', correct: null, type: 'multiple choice',
                explanation: ['The symbol presented is simply a symbol to represent a piece of statistics language.', '* The sigma symbol represents the spread of the population.', '* The s symbol (the one in our question) represents the spread of the sample.', '* The n symbol represents the number of units in the population or in the sample', '* The (sum-of) symbol represents a short-hand for the addition of a set of information']},
                {question: 'The image below contains a symbol commonly used in statistics. Which is the most appropriate definition?', image: '../../Images/n.png', answer1: 'It represents the number of units in a population or sample', answer2: 'It represents the spread that we see in a sample', answer3: 'It represents the variance that we see in a population', answer4: 'It represents the shorthand for summing a lot of information together', correct: null, type: 'multiple choice',
                explanation: ['The symbol presented is simply a symbol to represent a piece of statistics language.', '* The sigma symbol represents the spread of the population.', '* The s symbol represents the spread of the sample.', '* The n (the one in our question) symbol represents the number of units in the population or in the sample', '* The (sum-of) symbol represents a short-hand for the addition of a set of information']},
                {question: 'The image below contains a symbol commonly used in statistics. Which is the most appropriate definition?', image: '../../Images/sumof.png', answer1: 'It represents the shorthand for summing a lot of information together', answer2: 'It represents the spread that we see in a population', answer3: 'It represents the spread that we see in a sample', answer4: 'It represents the number of units in a population or sample', correct: null, type: 'multiple choice',
                explanation: ['The symbol presented is simply a symbol to represent a piece of statistics language.', '* The sigma symbol represents the spread of the population.', '* The s symbol represents the spread of the sample.', '* The n symbol represents the number of units in the population or in the sample', '* The (sum-of) symbol (the one in our question) represents a short-hand for the addition of a set of information']},
                {question: 'Which of the graphs below has the greatest (lowercase sigma)', image: '../../Images/distributions.png', answer1: null, answer2: null, answer3: null, answer4: null, correct: 'b', type: 'input',
                explanation: ['What is the question asking us? We are being asked what the spread in data is in our population', 'We know this because we\'re dealing with sigma (an analysis of population) and not s (an analysis of a sample)', 'So now, what does the greatest sigma mean - it means the largest spread in a population', 'Therefore we can rule our all sample graphs (D, E, and F)', 'Next we can rule out (A and C) because graph B has a greater distribution (it\'s wider) of data']},
                {question: 'Which of the graphs below has the lowest (lowercase sigma)', image: '../../Images/distributions.png', answer1: null, answer2: null, answer3: null, answer4: null, correct: 'c', type: 'input',
                explanation: ['What is the question asking us? We are being asked what the spread in data is in our population', 'We know this because we\'re dealing with sigma (an analysis of population) and not s (an analysis of a sample)', 'So now, what does the lowest sigma mean - it means the smallest spread in a population', 'Therefore we can rule our all sample graphs (D, E, and F)', 'Next we can rule out (A and B) because graph B has a lesser distribution (it\'s narrower) of data']},
                {question: 'Which of the graphs below has the greatest (s)', image: '../../Images/distributions.png', answer1: null, answer2: null, answer3: null, answer4: null, correct: 'e', type: 'input',
                explanation: ['What is the question asking us? We are being asked what the spread in data is in our sample', 'We know this because we\'re dealing with s (an analysis of sample) and not sigma (an analysis of a population)', 'So now, what does the greatest s mean - it means the largest spread in a sample', 'Therefore we can rule our all sample graphs (A, B, and C)', 'Next we can rule out (D and F) because graph E has a greater distribution (it\'s wider) of data']},
                {question: 'Which of the graphs below has the lowest (s)', image: '../../Images/distributions.png', answer1: null, answer2: null, answer3: null, answer4: null, correct: 'f', type: 'input',
                explanation: ['What is the question asking us? We are being asked what the spread in data is in our sample', 'We know this because we\'re dealing with s (an analysis of sample) and not sigma (an analysis of a population)', 'So now, what does the lowest s mean - it means the smallest spread in a sample', 'Therefore we can rule our all sample graphs (A, B, and C)', 'Next we can rule out (D and E) because graph F has a greater distribution (it\'s narrower) of data']},
                {question: 'What is the (n) of the population?', image: '../../Images/unitsum1.png', answer1: null, answer2: null, answer3: null, answer4: null, correct: '4', type: 'input',
                explanation: ['When dealing with the statistics symbol n, what we\'re dealing with is the number of units in a population or in a sample', 'Therefore what we\'re looking for is a count of the total units', 'in this case we are directed toward the population, so we\'re going to count our population units', 'A count gives us 4 units']},
                {question: 'What is the (n) of the sample?', image: '../../Images/unitsum1.png', answer1: null, answer2: null, answer3: null, answer4: null, correct: '2', type: 'input',
                explanation: ['When dealing with the statistics symbol n, what we\'re dealing with is the number of units in a population or in a sample', 'Therefore what we\'re looking for is a count of the total units', 'in this case we are directed toward the sample, so we\'re going to count our sample units', 'A count gives us 2 units']},
                {question: 'What is the (sumof) population dog lengths? (don\'t forget to include units)', image: '../../Images/unitsum1.png', answer1: null, answer2: null, answer3: null, answer4: null, correct: '191cm', type: 'input',
                explanation: ['So the information that we want is some information added together - more specifically that information is the length of dogs in our population', 'In this instance, we have 4 dogs in the population with lengths 60cm, 60cm, 30cm, and 41cm', 'Our (sum of) is therefore (60cm + 60cm + 30cm + 40cm) which is equal to 191cm']},
                {question: 'What is the (sumof) sample dog lengths? (don\'t forget to include units)', image: '../../Images/unitsum1.png', answer1: null, answer2: null, answer3: null, answer4: null, correct: '120cm', type: 'input',
                explanation: ['So the information that we want is some information added together - more specifically that information is the length of dogs in our sample', 'In this instance, we have 2 dogs in the sample with lengths 60cm, and 60cm', 'Our (sum of) is therefore (60cm + 60cm) which is equal to 120cm']},
                {question: 'What is the (n) of the population?', image: '../../Images/unitsum2.png', answer1: null, answer2: null, answer3: null, answer4: null, correct: '9', type: 'input',
                explanation: ['When dealing with the statistics symbol n, what we\'re dealing with is the number of units in a population or in a sample', 'Therefore what we\'re looking for is a count of the total units', 'in this case we are directed toward the population, so we\'re going to count our population units', 'A count gives us 9 units']},
                {question: 'What is the (n) of the sample?', image: '../../Images/unitsum2.png', answer1: null, answer2: null, answer3: null, answer4: null, correct: '5', type: 'input',
                explanation: ['When dealing with the statistics symbol n, what we\'re dealing with is the number of units in a population or in a sample', 'Therefore what we\'re looking for is a count of the total units', 'in this case we are directed toward the sample, so we\'re going to count our sample units', 'A count gives us 5 units']},
                {question: 'What is the (sumof) population pencil ratings? (don\'t forget to include units)', image: '../../Images/unitsum2.png', answer1: null, answer2: null, answer3: null, answer4: null, correct: '35', type: 'input',
                explanation: ['So the information that we want is some information added together - more specifically that information is the rating of pencils in our population', 'In this instance, we have 9 pencils in the population with ratings of 5, 4, 5, 5, 1, 2, 4, 5, and 4', 'Our (sum of) is therefore (5 + 4 + 5 + 5 + 1 + 2 + 4 + 5 + 4) which is equal to 35']},
                {question: 'What is the (sumof) sample pencil ratings? (don\'t forget to include units)', image: '../../Images/unitsum2.png', answer1: null, answer2: null, answer3: null, answer4: null, correct: '20', type: 'input',
                explanation: ['So the information that we want is some information added together - more specifically that information is the rating of pencils in our sample', 'In this instance, we have 5 pencils in the sample with ratings of 5, 4, 5, 5, and 1', 'Our (sum of) is therefore (5 + 4 + 5 + 5 + 1) which is equal to 20']},
                {question: 'What is the (n) of the population?', image: '../../Images/unitsum3.png', answer1: null, answer2: null, answer3: null, answer4: null, correct: '6', type: 'input',
                explanation: ['When dealing with the statistics symbol n, what we\'re dealing with is the number of units in a population or in a sample', 'Therefore what we\'re looking for is a count of the total units', 'in this case we are directed toward the population, so we\'re going to count our population units', 'A count gives us 6 units']},
                {question: 'What is the (n) of the sample?', image: '../../Images/unitsum3.png', answer1: null, answer2: null, answer3: null, answer4: null, correct: '5', type: 'input',
                explanation: ['When dealing with the statistics symbol n, what we\'re dealing with is the number of units in a population or in a sample', 'Therefore what we\'re looking for is a count of the total units', 'in this case we are directed toward the sample, so we\'re going to count our sample units', 'A count gives us 5 units']},
                {question: 'What is the (sumof) population romance genres? (don\'t forget to include units)', image: '../../Images/unitsum3.png', answer1: null, answer2: null, answer3: null, answer4: null, correct: '1', type: 'input',
                explanation: ['So the information that we want is some information added together - more specifically that information is the genre of romance movies in our population', 'In this instance, we have 1 movie in the population with genre \'Romance\'', 'Our (sum of) is therefore 1']},
                {question: 'What is the (sumof) sample comedy genres? (don\'t forget to include units)', image: '../../Images/unitsum3.png', answer1: null, answer2: null, answer3: null, answer4: null, correct: '2', type: 'input',
                explanation: ['So the information that we want is some information added together - more specifically that information is the genre of comedy movies in our sample', 'In this instance, we have 2 movies in the sample with genre \'Comedy\'', 'Our (sum of) is therefore (1 + 1) which is 2']},
            ];            
            break;        
        case "Reading%20Algebraic%20Notation%20Quiz%20III.html":            
            var askedquestions = [
                {question: 'The image below contains a symbol commonly used in statistics. Which is the most appropriate definition?', image: '../../Images/mu.png', answer1: 'The population parameter of interest', answer2: 'The sample statistic of interest', answer3: 'The proportion of an occurrence that we are looking for in the dataset of a population', answer4: 'The proportion of an occurrence that we are looking for in the dataset of a sample', correct: null, type: 'multiple choice', 
                explanation: ['The symbol presented is simply a symbol to represent a piece of statistics language.', '* The mu symbol (the one in our question) represents the population parameter of interest.', '* The x-bar symbol represents the sample statistic of interest.', '* The p symbol represents the population proportion of interest.', '* The p-hat symbol represents the sample proportion of interest.', '* The sigma symbol represents the spread of the population.', '* The s symbol represents the spread of the sample.', '* The n symbol represents the number of units in the population or in the sample', '* The (sum-of) symbol represents a short-hand for the addition of a set of information']},
                {question: 'The image below contains a symbol commonly used in statistics. Which is the most appropriate definition?', image: '../../Images/xbar.png', answer1: 'The sample statistic of interest', answer2: 'The population parameter of interest', answer3: 'The proportion of an occurrence that we are looking for in the dataset of a population', answer4: 'The proportion of an occurrence that we are looking for in the dataset of a sample', correct: null, type: 'multiple choice', 
                explanation: ['The symbol presented is simply a symbol to represent a piece of statistics language.', '* The mu symbol represents the population parameter of interest.', '* The x-bar symbol (the one in our question) represents the sample statistic of interest.', '* The p symbol represents the population proportion of interest.', '* The p-hat symbol represents the sample proportion of interest.', '* The sigma symbol represents the spread of the population.', '* The s symbol represents the spread of the sample.', '* The n symbol represents the number of units in the population or in the sample', '* The (sum-of) symbol represents a short-hand for the addition of a set of information']},
                {question: 'The image below contains a symbol commonly used in statistics. Which is the most appropriate definition?', image: '../../Images/p.png', answer1: 'The proportion of an occurrence that we are looking for in the dataset of a population', answer2: 'The sample statistic of interest', answer3: 'The population parameter of interest', answer4: 'The proportion of an occurrence that we are looking for in the dataset of a sample', correct: null, type: 'multiple choice', 
                explanation: ['The symbol presented is simply a symbol to represent a piece of statistics language.', '* The mu symbol represents the population parameter of interest.', '* The x-bar symbol represents the sample statistic of interest.', '* The p symbol (the one in our question) represents the population proportion of interest.', '* The p-hat symbol represents the sample proportion of interest.', '* The sigma symbol represents the spread of the population.', '* The s symbol represents the spread of the sample.', '* The n symbol represents the number of units in the population or in the sample', '* The (sum-of) symbol represents a short-hand for the addition of a set of information']},
                {question: 'The image below contains a symbol commonly used in statistics. Which is the most appropriate definition?', image: '../../Images/p-hat.png', answer1: 'The proportion of an occurrence that we are looking for in the dataset of a sample', answer2: 'The sample statistic of interest', answer3: 'The proportion of an occurrence that we are looking for in the dataset of a population', answer4: 'The population parameter of interest', correct: null, type: 'multiple choice', 
                explanation: ['The symbol presented is simply a symbol to represent a piece of statistics language.', '* The mu symbol represents the population parameter of interest.', '* The x-bar symbol represents the sample statistic of interest.', '* The p symbol represents the population proportion of interest.', '* The p-hat symbol (the one in our question) represents the sample proportion of interest.', '* The sigma symbol represents the spread of the population.', '* The s symbol represents the spread of the sample.', '* The n symbol represents the number of units in the population or in the sample', '* The (sum-of) symbol represents a short-hand for the addition of a set of information']},
                {question: 'The image below contains a symbol commonly used in statistics. Which is the most appropriate definition?', image: '../../Images/sigma.png', answer1: 'It represents the spread that we see in a population', answer2: 'It represents the spread that we see in a sample', answer3: 'It represents the variance that we see in a population', answer4: 'It represents the variance that we see in a sample', correct: null, type: 'multiple choice', 
                explanation: ['The symbol presented is simply a symbol to represent a piece of statistics language.', '* The mu symbol represents the population parameter of interest.', '* The x-bar symbol represents the sample statistic of interest.', '* The p symbol represents the population proportion of interest.', '* The p-hat symbol represents the sample proportion of interest.', '* The sigma symbol (the one in our question) represents the spread of the population.', '* The s symbol represents the spread of the sample.', '* The n symbol represents the number of units in the population or in the sample', '* The (sum-of) symbol represents a short-hand for the addition of a set of information']},
                {question: 'The image below contains a symbol commonly used in statistics. Which is the most appropriate definition?', image: '../../Images/s.png', answer1: 'It represents the spread that we see in a sample', answer2: 'It represents the spread that we see in a population', answer3: 'It represents the variance that we see in a population', answer4: 'It represents the variance that we see in a sample', correct: null, type: 'multiple choice', 
                explanation: ['The symbol presented is simply a symbol to represent a piece of statistics language.', '* The mu symbol represents the population parameter of interest.', '* The x-bar symbol represents the sample statistic of interest.', '* The p symbol represents the population proportion of interest.', '* The p-hat symbol represents the sample proportion of interest.', '* The sigma symbol represents the spread of the population.', '* The s symbol (the one in our question) represents the spread of the sample.', '* The n symbol represents the number of units in the population or in the sample', '* The (sum-of) symbol represents a short-hand for the addition of a set of information']},
                {question: 'The image below contains a symbol commonly used in statistics. Which is the most appropriate definition?', image: '../../Images/n.png', answer1: 'It represents the number of units in a population or sample', answer2: 'It represents the spread that we see in a sample', answer3: 'It represents the variance that we see in a population', answer4: 'It represents the shorthand for summing a lot of information together', correct: null, type: 'multiple choice', 
                explanation: ['The symbol presented is simply a symbol to represent a piece of statistics language.', '* The mu symbol represents the population parameter of interest.', '* The x-bar symbol represents the sample statistic of interest.', '* The p symbol represents the population proportion of interest.', '* The p-hat symbol represents the sample proportion of interest.', '* The sigma symbol represents the spread of the population.', '* The s symbol represents the spread of the sample.', '* The n symbol (the one in our question) represents the number of units in the population or in the sample', '* The (sum-of) symbol represents a short-hand for the addition of a set of information']},
                {question: 'The image below contains a symbol commonly used in statistics. Which is the most appropriate definition?', image: '../../Images/sumof.png', answer1: 'It represents the shorthand for summing a lot of information together', answer2: 'It represents the spread that we see in a population', answer3: 'It represents the spread that we see in a sample', answer4: 'It represents the number of units in a population or sample', correct: null, type: 'multiple choice', 
                explanation: ['The symbol presented is simply a symbol to represent a piece of statistics language.', '* The mu symbol represents the population parameter of interest.', '* The x-bar symbol represents the sample statistic of interest.', '* The p symbol represents the population proportion of interest.', '* The p-hat symbol represents the sample proportion of interest.', '* The sigma symbol represents the spread of the population.', '* The s symbol represents the spread of the sample.', '* The n symbol represents the number of units in the population or in the sample', '* The (sum-of) symbol (the one in our question) represents a short-hand for the addition of a set of information']},
                {question: 'What is the answer to this complex equation? (round to 2 decimal places)', image: '../../Images/complex1.png', answer1: null, answer2: null, answer3: null, answer4: null, correct: '8.08', type: 'input',
                explanation: ['Lets assess the components of this equation.', 'Firstly, what we mean by pp-hatn is p times p-hat times n. This is 0.5 * 0.6 * 10 or 0.3 * 10 so it\'s equal to 3', 'Next, what do we man by p/p-hatn. We mean p divided by the result of p-hat * n. This is equal to 0.5/(0.6 * 10) or 0.5/6. It is equal to 0.083333333333', 'Finally what do we mean by pn. We mean p times n. This equals 0.5 * 10 which is 5.', 'Lastly we need to combine the pieces. 3 + 0.083333333333 + 5 or 8.0833333333333', 'when rounded to two deicmal places, this is equal to 8.08']},
                {question: 'What is the answer to this complex equation? (round to 2 decimal places)', image: '../../Images/complex2.png', answer1: null, answer2: null, answer3: null, answer4: null, correct: '5', type: 'input',
                explanation: ['Lets assess the components of this equation.', 'In this instance, we\'re summing together information. The information is the index of x + 1 substracted by mean of dataset', 'So what is the value of the index of x + 1, well it\'s the value of each x in our dataset + 1. These would be 8, 9, 15, 16, and 22.', 'Next we need to calculate our mean. This is all values in our dataset divided by the number of values in our dataset. In our case (7 + 8 + 14 + 15 + 21)/5. This is equal to 65/5 or 13', 'Finally let\'s find the x index + 1 - mean values. These will be (8 - 13)(9 - 13)(15 - 13)(16 - 13)(22 - 13) or -5, -4, 2, 3, 9.', 'These values added together (the sum of) gives us 5']},
                {question: 'What is the answer to this complex equation? (answer as a fraction)', image: '../../Images/complex3.png', answer1: null, answer2: null, answer3: null, answer4: null, correct: '1/260', type: 'input',
                explanation: ['Lets assess the components of this equation.', 'This may look a little harder, but it isn\'t, everything is just in a fraction now. The process is still the same', 'So the first thing we need to work out is the value of what we mean by the first set of brackets musigma - xbars? Well it\'s mu * sigma minus x-bar * s', 'Well lets work this out, mu = 4 and sigma = 1, so this mu * sigma is equal to 4.', 'Next, x-bar = 6 and s = 0 so x-bar * s = 0', 'subtracting these gives us 4 - 0 which is 4.', 'Next we need to sum together the information of the x indexes. This is 7 + 8 + 14 + 15 + 21, which equals 65.', 'Our second to last step is to multiply our values together. In brackets we calculated mu * sigma minus x-bar * s, this value was 4. We also calculated our sum of the indexes of x, which was 65. multiplied together, these values equal 260.', 'This is the value of the denominator (bottom of the fraction). The overall answer is 1/260.']},
                {question: 'What is the answer to this complex equation? (round to 2 decimal places)', image: '../../Images/complex4.png', answer1: null, answer2: null, answer3: null, answer4: null, correct: '65', type: 'input',
                explanation: ['Lets assess the components of this equation.', 'What we need to do here is add together the symbols p, p-hat, n, mu, x-bar, sigma, and s', 'Note that we only need to add them once because even though we have a sum of, the number of items in each values index is only one', '(there is only one value for p, and one value for p-hat, ... so on and so forth)', 'So let\'s add them together, 1 + 8 + 7 + 17 + 9 + 4 + 19', 'This adds up to 65']},
            ];
            break;
        case "Graphs%20Quiz.html":
            var askedquestions = [
                {question: 'Which of these is an example of univariate data?', image: '../../Images/unimultitime1.png', answer1: 'a', answer2: 'b', answer3: 'c', answer4: 'None', correct: null, type: 'multiple choice',
                explanation: ['We need to ascertain which of these defnitions is most approriate for our data', 'Univariate data: We\'re only concerned with measuring a single variable for each unit', 'Multivariate data: We\'re concerned with measuring two or more variables for each of our units', 'Time series data: We may have one more variables of data that we\'re measuring over time', 'In this case the most appropriate would be Univariate data as each unit has only a single variable measured at one time']},
                {question: 'Which of these is an example of univariate data?', image: '../../Images/unimultitime2.png', answer1: 'a', answer2: 'b', answer3: 'c', answer4: 'None', correct: null, type: 'multiple choice',
                explanation: ['We need to ascertain which of these defnitions is most approriate for our data', 'Univariate data: We\'re only concerned with measuring a single variable for each unit', 'Multivariate data: We\'re concerned with measuring two or more variables for each of our units', 'Time series data: We may have one more variables of data that we\'re measuring over time', 'In this case the most appropriate would be Univariate data as each unit has only a single variable measured at one time']},
                {question: 'Which of these is an example of univariate data?', image: '../../Images/unimultitime3.png', answer1: 'c', answer2: 'a', answer3: 'b', answer4: 'None', correct: null, type: 'multiple choice',
                explanation: ['We need to ascertain which of these defnitions is most approriate for our data', 'Univariate data: We\'re only concerned with measuring a single variable for each unit', 'Multivariate data: We\'re concerned with measuring two or more variables for each of our units', 'Time series data: We may have one more variables of data that we\'re measuring over time', 'In this case the most appropriate would be Univariate data as each unit has only a single variable measured at one time']},
                {question: 'Which of these is an example of multivariate data?', image: '../../Images/unimultitime1.png', answer1: 'b', answer2: 'a', answer3: 'c', answer4: 'None', correct: null, type: 'multiple choice',
                explanation: ['We need to ascertain which of these defnitions is most approriate for our data', 'Univariate data: We\'re only concerned with measuring a single variable for each unit', 'Multivariate data: We\'re concerned with measuring two or more variables for each of our units', 'Time series data: We may have one more variables of data that we\'re measuring over time', 'In this case the most appropriate would be Multivariate data as each unit has more than one variable measured at one time']},
                {question: 'Which of these is an example of multivariate data?', image: '../../Images/unimultitime2.png', answer1: 'c', answer2: 'a', answer3: 'c', answer4: 'None', correct: null, type: 'multiple choice',
                explanation: ['We need to ascertain which of these defnitions is most approriate for our data', 'Univariate data: We\'re only concerned with measuring a single variable for each unit', 'Multivariate data: We\'re concerned with measuring two or more variables for each of our units', 'Time series data: We may have one more variables of data that we\'re measuring over time', 'In this case the most appropriate would be Multivariate data as each unit has more than one variable measured at one time']},
                {question: 'Which of these is an example of multivariate data?', image: '../../Images/unimultitime3.png', answer1: 'a', answer2: 'b', answer3: 'c', answer4: 'None', correct: null, type: 'multiple choice',
                explanation: ['We need to ascertain which of these defnitions is most approriate for our data', 'Univariate data: We\'re only concerned with measuring a single variable for each unit', 'Multivariate data: We\'re concerned with measuring two or more variables for each of our units', 'Time series data: We may have one more variables of data that we\'re measuring over time', 'In this case the most appropriate would be Multivariate data as each unit has more than one variable measured at one time']},
                {question: 'Which of these is an example of time series data?', image: '../../Images/unimultitime1.png', answer1: 'c', answer2: 'a', answer3: 'c', answer4: 'None', correct: null, type: 'multiple choice',
                explanation: ['We need to ascertain which of these defnitions is most approriate for our data', 'Univariate data: We\'re only concerned with measuring a single variable for each unit', 'Multivariate data: We\'re concerned with measuring two or more variables for each of our units', 'Time series data: We may have one more variables of data that we\'re measuring over time', 'In this case the most appropriate would be Time series data as each unit is measured at multiple periods of time']},
                {question: 'Which of these is an example of time series data?', image: '../../Images/unimultitime2.png', answer1: 'b', answer2: 'a', answer3: 'c', answer4: 'None', correct: null, type: 'multiple choice',
                explanation: ['We need to ascertain which of these defnitions is most approriate for our data', 'Univariate data: We\'re only concerned with measuring a single variable for each unit', 'Multivariate data: We\'re concerned with measuring two or more variables for each of our units', 'Time series data: We may have one more variables of data that we\'re measuring over time', 'In this case the most appropriate would be Time series data as each unit is measured at multiple periods of time']},
                {question: 'Which of these is an example of time series data?', image: '../../Images/unimultitime3.png', answer1: 'b', answer2: 'a', answer3: 'c', answer4: 'None', correct: null, type: 'multiple choice',
                explanation: ['We need to ascertain which of these defnitions is most approriate for our data', 'Univariate data: We\'re only concerned with measuring a single variable for each unit', 'Multivariate data: We\'re concerned with measuring two or more variables for each of our units', 'Time series data: We may have one more variables of data that we\'re measuring over time', 'In this case the most appropriate would be Time series data as each unit is measured at multiple periods of time']},
                {question: 'One of the following answers is a qualitative variable, which one is it?', image: '../../Images/qual1.png', answer1: 'a', answer2: 'b', answer3: 'c', answer4: 'd', correct: null, type: 'multiple choice',
                explanation: ['For a question like this, we should concern ourselves with only one question: If I list endlessly, will I eventually run out of items to list? (is the set of information finite)', 'If yes, then we\'re dealing with a qualitative (categorical) variable', 'If not, we\'re dealing with a quantitative (continuous) variable', 'In this case, our qualitative variable is wine as there is a finite number of possible wines']},
                {question: 'One of the following answers is a qualitative variable, which one is it?', image: '../../Images/qual2.png', answer1: 'c', answer2: 'b', answer3: 'a', answer4: 'd', correct: null, type: 'multiple choice',
                explanation: ['For a question like this, we should concern ourselves with only one question: If I list endlessly, will I eventually run out of items to list? (is the set of information finite)', 'If yes, then we\'re dealing with a qualitative (categorical) variable', 'If not, we\'re dealing with a quantitative (continuous) variable', 'In this case, our qualitative variable is cars as there is a finite number of possible cars']},
                {question: 'One of the following answers is a qualitative variable, which one is it?', image: '../../Images/qual3.png', answer1: 'd', answer2: 'b', answer3: 'a', answer4: 'c', correct: null, type: 'multiple choice',
                explanation: ['For a question like this, we should concern ourselves with only one question: If I list endlessly, will I eventually run out of items to list? (is the set of information finite)', 'If yes, then we\'re dealing with a qualitative (categorical) variable', 'If not, we\'re dealing with a quantitative (continuous) variable', 'In this case, our qualitative variable is juice as there is a finite number of possible juices']},
                {question: 'One of the following answers is a quantitative variable, which one is it?', image: '../../Images/quan1.png', answer1: 'b', answer2: 'd', answer3: 'a', answer4: 'c', correct: null, type: 'multiple choice',
                explanation: ['For a question like this, we should concern ourselves with only one question: If I list endlessly, will I eventually run out of items to list? (is the set of information finite)', 'If yes, then we\'re dealing with a qualitative (categorical) variable', 'If not, we\'re dealing with a quantitative (continuous) variable', 'In this case, our qualitative variable is distance as there is a infinite number of possible distances']},
                {question: 'One of the following answers is a quantitative variable, which one is it?', image: '../../Images/quan2.png', answer1: 'a', answer2: 'd', answer3: 'c', answer4: 'b', correct: null, type: 'multiple choice',
                explanation: ['For a question like this, we should concern ourselves with only one question: If I list endlessly, will I eventually run out of items to list? (is the set of information finite)', 'If yes, then we\'re dealing with a qualitative (categorical) variable', 'If not, we\'re dealing with a quantitative (continuous) variable', 'In this case, our qualitative variable is weight as there is a infinite number of possible weights']},
                {question: 'One of the following answers is a quantitative variable, which one is it?', image: '../../Images/quan3.png', answer1: 'c', answer2: 'd', answer3: 'a', answer4: 'b', correct: null, type: 'multiple choice',
                explanation: ['For a question like this, we should concern ourselves with only one question: If I list endlessly, will I eventually run out of items to list? (is the set of information finite)', 'If yes, then we\'re dealing with a qualitative (categorical) variable', 'If not, we\'re dealing with a quantitative (continuous) variable', 'In this case, our qualitative variable is velocity as there is a infinite number of possible velocities']},
                {question: 'What kind of information do we represent with a pie chart?', image: null, answer1: 'We usually look to compare information within a single categorical vairable', answer2: 'We usually look to compare information between multiple categorical variables', answer3: 'We want to compare the frequency of occurrences', answer4: 'We want to compare total frequency changes', correct: null, type: 'multiple choice',
                explanation: ['When dealing with a pie chart, in general we look to include the units within a single measured categorical variable']},
                {question: 'What kind of information do we represent with a bar graph?', image: null, answer1: 'We usually look to compare information between multiple categorical variables', answer2: 'We usually look to compare information within a single categorical vairable', answer3: 'We want to compare the frequency of occurrences', answer4: 'We want to compare total frequency changes', correct: null, type: 'multiple choice',
                explanation: ['With a bar we look to compare information from one or more categorical variables, but most often multiple categorical variables']},
                {question: 'Which of these is not a feature of a pie chart?', image: null, answer1: 'labelled x and y axes', answer2: 'usually deals with a single categorical variable', answer3: 'circular in shape', answer4: 'a comparison of proportions as a whole', correct: null, type: 'multiple choice',
                explanation: ['The standard, odd one out style of question. In our case, we want to find what is part of a pie chart, so we can see what remains', 'Well pie charts are usually circular in shape (like pies)', 'It compares the proportions of units to each other within a single categorical variable', 'It normally deals with a single categorical variable', 'therefore our remainder, labelled x and y axis is incorrect - because a pie chart doesn\'t have x and y-axis']},
                {question: 'Which of these is not a feature of a bar graph?', image: null, answer1: 'bar graphs can handle large sources of data', answer2: 'spaces between the bars', answer3: 'labelled x and y axes', answer4: 'usually compares multiple categorical variables or a categorical variable over time', correct: null, type: 'multiple choice',
                explanation: ['The standard, odd one out style of question. In our case, we want to find what is part of a bar graph, so we can see what remains', 'A bar chart does have spaces between the bars', 'A bar char does have a labelled x and y axes', 'A bar chart is usually used to compare categorical variables', 'therefore bar charts cannot handle large sources of data (because they quickly run our of horizontal space)']},
                {question: 'What is wrong with the following graph?', image: '../../Images/simple1.png', answer1: 'too simple', answer2: 'too complex', answer3: 'non-zero axis', answer4: 'it\'s irrelevant', correct: null, type: 'multiple choice',
                explanation: ['When analysing a graph we need to do a checklist', 'Is it too simple', 'Is it too complex', 'Does it have a non-zero axis', 'Is the information relevant to each other', 'Are the proportions correct', 'Are the x and y axes inconsistent with their numbers increasing', 'In this case, the graph is too simple']},
                {question: 'What is wrong with the following graph?', image: '../../Images/simple2.png', answer1: 'too simple', answer2: 'the comparative proportions', answer3: 'inconsistent axis', answer4: 'too complex', correct: null, type: 'multiple choice',
                explanation: ['When analysing a graph we need to do a checklist', 'Is it too simple', 'Is it too complex', 'Does it have a non-zero axis', 'Is the information relevant to each other', 'Are the proportions correct', 'Are the x and y axes inconsistent with their numbers increasing', 'In this case, the graph is too simple']},
                {question: 'What is wrong with the following graph?', image: '../../Images/complexgraph1.png', answer1: 'too complex', answer2: 'too simple', answer3: 'non-zero axis', answer4: 'it\'s irrelevant', correct: null, type: 'multiple choice',
                explanation: ['When analysing a graph we need to do a checklist', 'Is it too simple', 'Is it too complex', 'Does it have a non-zero axis', 'Is the information relevant to each other', 'Are the proportions correct', 'Are the x and y axes inconsistent with their numbers increasing', 'In this case, the graph is too complex']},
                {question: 'What is wrong with the following graph?', image: '../../Images/complexgraph2.png', answer1: 'too complex', answer2: 'the comparative proportions', answer3: 'inconsistent axis', answer4: 'too simple', correct: null, type: 'multiple choice',
                explanation: ['When analysing a graph we need to do a checklist', 'Is it too simple', 'Is it too complex', 'Does it have a non-zero axis', 'Is the information relevant to each other', 'Are the proportions correct', 'Are the x and y axes inconsistent with their numbers increasing', 'In this case, the graph is too complex']},
                {question: 'What is wrong with the following graph?', image: '../../Images/nonzero1.png', answer1: 'non-zero axis', answer2: 'too complex', answer3: 'it\'s irrelevant', answer4: 'the comparative proportions', correct: null, type: 'multiple choice',
                explanation: ['When analysing a graph we need to do a checklist', 'Is it too simple', 'Is it too complex', 'Does it have a non-zero axis', 'Is the information relevant to each other', 'Are the proportions correct', 'Are the x and y axes inconsistent with their numbers increasing', 'In this case, the graph has a non-zero axis']},
                {question: 'What is wrong with the following graph?', image: '../../Images/nonzero2.png', answer1: 'non-zero axis', answer2: 'inconsistent axis', answer3: 'too simple', answer4: 'too complex', correct: null, type: 'multiple choice',
                explanation: ['When analysing a graph we need to do a checklist', 'Is it too simple', 'Is it too complex', 'Does it have a non-zero axis', 'Is the information relevant to each other', 'Are the proportions correct', 'Are the x and y axes inconsistent with their numbers increasing', 'In this case, the graph has a non-zero axis']},
                {question: 'What is wrong with the following graph?', image: '../../Images/relevance1.png', answer1: 'it\'s irrelevant', answer2: 'non-zero axis', answer3: 'the comparative proportions', answer4: 'inconsistent axis', correct: null, type: 'multiple choice',
                explanation: ['When analysing a graph we need to do a checklist', 'Is it too simple', 'Is it too complex', 'Does it have a non-zero axis', 'Is the information relevant to each other', 'Are the proportions correct', 'Are the x and y axes inconsistent with their numbers increasing', 'In this case, the graph is irrelevant']},
                {question: 'What is wrong with the following graph?', image: '../../Images/relevance2.png', answer1: 'it\'s irrelevant', answer2: 'too simple', answer3: 'too complex', answer4: 'non-zero axis', correct: null, type: 'multiple choice',
                explanation: ['When analysing a graph we need to do a checklist', 'Is it too simple', 'Is it too complex', 'Does it have a non-zero axis', 'Is the information relevant to each other', 'Are the proportions correct', 'Are the x and y axes inconsistent with their numbers increasing', 'In this case, the graph is irrelevant']},
                {question: 'What is wrong with the following graph?', image: '../../Images/picto1.png', answer1: 'the comparative proportions', answer2: 'it\'s irrelevant', answer3: 'inconsistent axis', answer4: 'too simple', correct: null, type: 'multiple choice',
                explanation: ['When analysing a graph we need to do a checklist', 'Is it too simple', 'Is it too complex', 'Does it have a non-zero axis', 'Is the information relevant to each other', 'Are the proportions correct', 'Are the x and y axes inconsistent with their numbers increasing', 'In this case, the graph doesn\'t correctly represent proportions']},
                {question: 'What is wrong with the following graph?', image: '../../Images/picto2.png', answer1: 'the comparative proportions', answer2: 'too complex', answer3: 'non-zero axis', answer4: 'it\'s irrelevant', correct: null, type: 'multiple choice',
                explanation: ['When analysing a graph we need to do a checklist', 'Is it too simple', 'Is it too complex', 'Does it have a non-zero axis', 'Is the information relevant to each other', 'Are the proportions correct', 'Are the x and y axes inconsistent with their numbers increasing', 'In this case, the graph doesn\'t correctly represent proportions']},
                {question: 'What is wrong with the following graph?', image: '../../Images/inconsistent1.png', answer1: 'inconsistent axis', answer2: 'too simple', answer3: 'too complex', answer4: 'non-zero axis', correct: null, type: 'multiple choice',
                explanation: ['When analysing a graph we need to do a checklist', 'Is it too simple', 'Is it too complex', 'Does it have a non-zero axis', 'Is the information relevant to each other', 'Are the proportions correct', 'Are the x and y axes inconsistent with their numbers increasing', 'In this case, the graph has an x or y axes that doesn\'t increase at a consistent rate']},
                {question: 'What is wrong with the following graph?', image: '../../Images/inconsistent2.png', answer1: 'inconsistent axis', answer2: 'it\'s irrelevant', answer3: 'the comparative proportions', answer4: 'too simple', correct: null, type: 'multiple choice',
                explanation: ['When analysing a graph we need to do a checklist', 'Is it too simple', 'Is it too complex', 'Does it have a non-zero axis', 'Is the information relevant to each other', 'Are the proportions correct', 'Are the x and y axes inconsistent with their numbers increasing', 'In this case, the graph has an x or y axes that doesn\'t increase at a consistent rate']},
            ];
            break;
        case "Histograms%20Quiz.html":
            var askedquestions = [
                {question: 'What feature of a histogram is circled in red?', image: '../../Images/histo1.png', answer1: 'title and x-axis', answer2: 'frequency (y-axis)', answer3: 'bins', answer4: 'cumulative frequency', correct: null, type: 'multiple choice',
                explanation: ['What are the features of a histogram that we could circle?', 'The title', 'The x-axis', 'The y-axis (frequency)', 'the bins', 'the joint bars (unlike a bar graph)', 'In this case we\'ve circled the title and x-axis']},
                {question: 'What feature of a histogram is circled in red?', image: '../../Images/histo2.png', answer1: 'frequency (y-axis)', answer2: 'title and x-axis', answer3: 'bins', answer4: 'cumulative frequency', correct: null, type: 'multiple choice',
                explanation: ['What are the features of a histogram that we could circle?', 'The title', 'The x-axis', 'The y-axis (frequency)', 'the bins', 'the joint bars (unlike a bar graph)', 'In this case we\'ve circled the y-axis (frequency)']},
                {question: 'What feature of a histogram is circled in red?', image: '../../Images/histo3.png', answer1: 'bins', answer2: 'title and x-axis', answer3: 'frequency (y-axis)', answer4: 'cumulative frequency', correct: null, type: 'multiple choice',
                explanation: ['What are the features of a histogram that we could circle?', 'The title', 'The x-axis', 'The y-axis (frequency)', 'the bins', 'the joint bars (unlike a bar graph)', 'In this case we\'ve circled the bins']},
                {question: 'What feature of a histogram is circled in red?', image: '../../Images/histo4.png', answer1: 'title and x-axis', answer2: 'frequency (y-axis)', answer3: 'bins', answer4: 'cumulative frequency', correct: null, type: 'multiple choice',
                explanation: ['What are the features of a histogram that we could circle?', 'The title', 'The x-axis', 'The y-axis (frequency)', 'the bins', 'the joint bars (unlike a bar graph)', 'In this case we\'ve circled the title and x-axis']},
                {question: 'What feature of a histogram is circled in red?', image: '../../Images/histo5.png', answer1: 'frequency (y-axis)', answer2: 'title and x-axis', answer3: 'bins', answer4: 'cumulative frequency', correct: null, type: 'multiple choice',
                explanation: ['What are the features of a histogram that we could circle?', 'The title', 'The x-axis', 'The y-axis (frequency)', 'the bins', 'the joint bars (unlike a bar graph)', 'In this case we\'ve circled the y-axis (frequency)']},
                {question: 'What feature of a histogram is circled in red?', image: '../../Images/histo6.png', answer1: 'bins', answer2: 'title and x-axis', answer3: 'frequency (y-axis)', answer4: 'cumulative frequency', correct: null, type: 'multiple choice',
                explanation: ['What are the features of a histogram that we could circle?', 'The title', 'The x-axis', 'The y-axis (frequency)', 'the bins', 'the joint bars (unlike a bar graph)', 'In this case we\'ve circled the bins']},
                {question: 'What is a good estimate of the frequency of best actress winners whos age is greater than 40?', image: '../../Images/histo7.png', answer1: '19', answer2: '24', answer3: '12', answer4: '15', correct: null, type: 'multiple choice',
                explanation: ['We need to begin this question by figuring the frequency of all the bars', 'Bin (20 - 25) = 7.5, Bin (25 - 30) = 22.5, Bin (30 - 35) = 27.5, Bin (35 - 40) = 17.5, Bin (40 - 45) = 7.5, Bin (45 - 50) = 7.5, Bin (50 - 55) = 0, Bin (55 - 60) = 0, Bin (60 - 65) = 5, Bin (65 - 70) = 0, Bin (70 - 75) = 1, Bin (75 - 80) = 0, Bin (80 - 85) = 1', 'Now the frequency of values that are 40 or greater is 7.5 + 7.5 + 5 + 1 + 1 or 22', 'Note that the y-axis (frequency is measured in percent. So this represents 22% of units.', 'The n (as labelled in the title) is equal to 83. So we want to know what 22% of 83 is.', 'Our answer is 18.26 but we round up because you can\'t have .26 of a person.', 'Our answer is 19']},
                {question: 'What is a good estimate of the frequency of best actress winners whos aged between 25 and 35?', image: '../../Images/histo7.png', answer1: '42', answer2: '51', answer3: '33', answer4: '27', correct: null, type: 'multiple choice',
                explanation: ['We need to begin this question by figuring the frequency of all the bars', 'Bin (20 - 25) = 7.5, Bin (25 - 30) = 22.5, Bin (30 - 35) = 27.5, Bin (35 - 40) = 17.5, Bin (40 - 45) = 7.5, Bin (45 - 50) = 7.5, Bin (50 - 55) = 0, Bin (55 - 60) = 0, Bin (60 - 65) = 5, Bin (65 - 70) = 0, Bin (70 - 75) = 1, Bin (75 - 80) = 0, Bin (80 - 85) = 1', 'Now the frequency of values that are between 25 and 35 is 22.5 + 27.5 or 50', 'Note that the y-axis (frequency is measured in percent. So this represents 50% of units.', 'The n (as labelled in the title) is equal to 83. So we want to know what 50% of 83 is.', 'Our answer is 41.5 but we round up because you can\'t have .5 of a person', 'Our answer is 42']},
                {question: 'What is a good estimate of the frequency of tire pressures less than and equal to 34.75?', image: '../../Images/histo8.png', answer1: '22', answer2: '11', answer3: '28', answer4: '17', correct: null, type: 'multiple choice',
                explanation: ['We need to begin this question by figuring the frequency of all the bars', 'Bin (32.75 - 33.25) = 1, Bin (33.25 - 33.75) = 3, Bin (33.75 - 34.25) = 8, Bin (34.25 - 34.75) = 10, Bin (34.75 - 35.25) = 8, Bin (35.25 - 35.75) = 8, Bin (35.75 - 36.25) = 7, Bin (36.25 - 36.75) = 2, Bin (36.75 - 37.25) = 2, Bin (37.25 - 37.75) = 1', 'Now the frequency of values that are less than or equal to 34.75 are 10 + 8 + 3 + 1 or 22', 'Therefore our answer is 22']},
                {question: 'What is a good estimate of the frequency of tire pressures between 35.75 and 37.25?', image: '../../Images/histo8.png', answer1: '11', answer2: '22', answer3: '16', answer4: '28', correct: null, type: 'multiple choice',
                explanation: ['We need to begin this question by figuring the frequency of all the bars', 'Bin (32.75 - 33.25) = 1, Bin (33.25 - 33.75) = 3, Bin (33.75 - 34.25) = 8, Bin (34.25 - 34.75) = 10, Bin (34.75 - 35.25) = 8, Bin (35.25 - 35.75) = 8, Bin (35.75 - 36.25) = 7, Bin (36.25 - 36.75) = 2, Bin (36.75 - 37.25) = 2, Bin (37.25 - 37.75) = 1', 'Now the frequency of values that are between 35.75 and 37.25 is 7 + 2 + 2 or 11', 'Therefore our answer is 11']},
                {question: 'Which of these is a cumulative frequency histogram?', image: '../../Images/cumul1.png', answer1: 'b', answer2: 'a', answer3: 'we cannot tell', answer4: 'neither', correct: null, type: 'multiple choice',
                explanation: ['Our answer is going to be the graph that never has bins that decrease in frequency', 'Our answer is therefore B, because bin 2 for graph A decreases']},
                {question: 'Which of these is a cumulative frequency histogram?', image: '../../Images/cumul2.png', answer1: 'c', answer2: 'd', answer3: 'we cannot tell', answer4: 'neither', correct: null, type: 'multiple choice',
                explanation: ['Our answer is going to be the graph that never has bins that decrease in frequency', 'Our answer is therefore C, because bin 2 for graph D decreases']},
                {question: 'Which of these is a cumulative frequency histogram?', image: '../../Images/cumul3.png', answer1: 'we cannot tell', answer2: 'e', answer3: 'f', answer4: 'neither', correct: null, type: 'multiple choice',
                explanation: ['Our answer is going to be the graph that never has bins that decrease in frequency', 'We don\'t know what our answer is because both graphs have the same bins. We can\'t discern one not being a cumulative frequency histogram']},
                {question: 'If you have 6.5 bottles which percentile of bottle owners are you in?', image: '../../Images/ogive1.png', answer1: '60th', answer2: '100th', answer3: '8th', answer4: '40th', correct: null, type: 'multiple choice',
                explanation: ['An ogive is the line that goes from bin to bin on a cumulative frequency histogram', 'At each point the measure of its y-axis represents the percentile in the data', 'Of the answers available 6.5 bottles looks to measure to the 60th percentile']},
                {question: 'If you have 9 bottles which percentile of bottle owners are you in?', image: '../../Images/ogive1.png', answer1: '100th', answer2: '60th', answer3: '8th', answer4: '40th', correct: null, type: 'multiple choice',
                explanation: ['An ogive is the line that goes from bin to bin on a cumulative frequency histogram', 'At each point the measure of its y-axis represents the percentile in the data', 'Of the answers available 9 bottles looks to measure to the 100th percentile']},
                {question: 'If you have 3 bottles which percentile of bottle owners are you in?', image: '../../Images/ogive1.png', answer1: '8th', answer2: '60th', answer3: '100th', answer4: '40th', correct: null, type: 'multiple choice',
                explanation: ['An ogive is the line that goes from bin to bin on a cumulative frequency histogram', 'At each point the measure of its y-axis represents the percentile in the data', 'Of the answers available 3 bottles looks to measure to the 8th percentile']},
            ];
            break;
        case "Plots%20Quiz.html":
            var askedquestions = [
                {question: 'What is the circled value?', image: '../../Images/boxmin1.png', answer1: 'min', answer2: 'Q1', answer3: 'median', answer4: 'Q3', correct: null, type: 'multiple choice',
                explanation: ['In a box plot, we represent information from a 5 number summary. Those numbers are:', 'The minimum', 'The 1st Quartile', 'The median (or secod quartile)', 'The 3rd Quartile', 'The maximum', 'Our circled value is the minimum']},
                {question: 'What is the circled value?', image: '../../Images/boxmin2.png', answer1: 'min', answer2: 'max', answer3: 'Q1', answer4: 'median', correct: null, type: 'multiple choice',
                explanation: ['In a box plot, we represent information from a 5 number summary. Those numbers are:', 'The minimum', 'The 1st Quartile', 'The median (or secod quartile)', 'The 3rd Quartile', 'The maximum', 'Our circled value is the minimum']},
                {question: 'What is the circled value?', image: '../../Images/boxq1-1.png', answer1: 'Q1', answer2: 'Q3', answer3: 'max', answer4: 'min', correct: null, type: 'multiple choice',
                explanation: ['In a box plot, we represent information from a 5 number summary. Those numbers are:', 'The minimum', 'The 1st Quartile', 'The median (or secod quartile)', 'The 3rd Quartile', 'The maximum', 'Our circled value is the 1st quartile']},,
                {question: 'What is the circled value?', image: '../../Images/boxq1-2.png', answer1: 'Q1', answer2: 'median', answer3: 'Q3', answer4: 'max', correct: null, type: 'multiple choice',
                explanation: ['In a box plot, we represent information from a 5 number summary. Those numbers are:', 'The minimum', 'The 1st Quartile', 'The median (or secod quartile)', 'The 3rd Quartile', 'The maximum', 'Our circled value is the 1st quartile']},
                {question: 'What is the circled value?', image: '../../Images/boxq2-1.png', answer1: 'median', answer2: 'min', answer3: 'Q1', answer4: 'Q3', correct: null, type: 'multiple choice',
                explanation: ['In a box plot, we represent information from a 5 number summary. Those numbers are:', 'The minimum', 'The 1st Quartile', 'The median (or secod quartile)', 'The 3rd Quartile', 'The maximum', 'Our circled value is the median']},
                {question: 'What is the circled value?', image: '../../Images/boxq2-2.png', answer1: 'median', answer2: 'max', answer3: 'min', answer4: 'Q1', correct: null, type: 'multiple choice',
                explanation: ['In a box plot, we represent information from a 5 number summary. Those numbers are:', 'The minimum', 'The 1st Quartile', 'The median (or secod quartile)', 'The 3rd Quartile', 'The maximum', 'Our circled value is the median']},
                {question: 'What is the circled value?', image: '../../Images/boxq3-1.png', answer1: 'Q3', answer2: 'median', answer3: 'max', answer4: 'min', correct: null, type: 'multiple choice',
                explanation: ['In a box plot, we represent information from a 5 number summary. Those numbers are:', 'The minimum', 'The 1st Quartile', 'The median (or secod quartile)', 'The 3rd Quartile', 'The maximum', 'Our circled value is the 3rd quartile']},
                {question: 'What is the circled value?', image: '../../Images/boxq3-2.png', answer1: 'Q3', answer2: 'Q1', answer3: 'median', answer4: 'max', correct: null, type: 'multiple choice',
                explanation: ['In a box plot, we represent information from a 5 number summary. Those numbers are:', 'The minimum', 'The 1st Quartile', 'The median (or secod quartile)', 'The 3rd Quartile', 'The maximum', 'Our circled value is the 3rd quartile']},
                {question: 'What is the circled value?', image: '../../Images/boxmax1.png', answer1: 'max', answer2: 'min', answer3: 'Q1', answer4: 'median', correct: null, type: 'multiple choice',
                explanation: ['In a box plot, we represent information from a 5 number summary. Those numbers are:', 'The minimum', 'The 1st Quartile', 'The median (or secod quartile)', 'The 3rd Quartile', 'The maximum', 'Our circled value is the maximum']},
                {question: 'What is the circled value?', image: '../../Images/boxmax2.png', answer1: 'max', answer2: 'Q3', answer3: 'min', answer4: 'Q1', correct: null, type: 'multiple choice',
                explanation: ['In a box plot, we represent information from a 5 number summary. Those numbers are:', 'The minimum', 'The 1st Quartile', 'The median (or secod quartile)', 'The 3rd Quartile', 'The maximum', 'Our circled value is the maximum']},
                {question: 'What is the approximate value of the min?', image: '../../Images/boxplot1.png', answer1: null, answer2: null, answer3: null, answer4: null, correct: '2', type: 'input',
                explanation: ['The minimum of a box plot is the value we represent with the first bar', 'In this case, that value is approximately equal to 2']},
                {question: 'What is the approximate value of the min? (round to the nearest ten)', image: '../../Images/boxplot2.png', answer1: null, answer2: null, answer3: null, answer4: null, correct: '20', type: 'input',
                explanation: ['The minimum of a box plot is the value we represent with the first bar', 'In this case, that value when rounded to the nearest ten is approximately equal to 2']},
                {question: 'What is the approximate value of Q1?', image: '../../Images/boxplot1.png', answer1: null, answer2: null, answer3: null, answer4: null, correct: '3', type: 'input',
                explanation: ['The Q1 of a box plot is the value we represent with the second bar', 'In this case, that value is approximately equal to 3']},
                {question: 'What is the approximate value of Q1? (round to the nearest ten)', image: '../../Images/boxplot2.png', answer1: null, answer2: null, answer3: null, answer4: null, correct: '30', type: 'input',
                explanation: ['The Q1 of a box plot is the value we represent with the second bar', 'In this case, that value when rounded to the nearest ten is approximately equal to 30']},
                {question: 'What is the approximate value of the median?', image: '../../Images/boxplot1.png', answer1: null, answer2: null, answer3: null, answer4: null, correct: '6', type: 'input',
                explanation: ['The median of a box plot is the value we represent with the third bar', 'In this case, that value is approximately equal to 6']},
                {question: 'What is the approximate value of the median? (round to the nearest ten)', image: '../../Images/boxplot2.png', answer1: null, answer2: null, answer3: null, answer4: null, correct: '60', type: 'input',
                explanation: ['The median of a box plot is the value we represent with the third bar', 'In this case, that value when rounded to the nearest ten is approximately equal to 60']},
                {question: 'What is the approximate value of Q3?', image: '../../Images/boxplot1.png', answer1: null, answer2: null, answer3: null, answer4: null, correct: '8', type: 'input',
                explanation: ['The Q3 of a box plot is the value we represent with the fourth bar', 'In this case, that value is approximately equal to 8']},
                {question: 'What is the approximate value of Q3? (round to the nearest ten)', image: '../../Images/boxplot2.png', answer1: null, answer2: null, answer3: null, answer4: null, correct: '70', type: 'input',
                explanation: ['The Q3 of a box plot is the value we represent with the fourth bar', 'In this case, that value when rounded to the nearest ten is approximately equal to 70']},
                {question: 'What is the approximate value of the max?', image: '../../Images/boxplot1.png', answer1: null, answer2: null, answer3: null, answer4: null, correct: '10', type: 'input',
                explanation: ['The maximum of a box plot is the value we represent with the last bar', 'In this case, that value is approximately equal to 10']},
                {question: 'What is the approximate value of the max? (round to the nearest ten)', image: '../../Images/boxplot2.png', answer1: null, answer2: null, answer3: null, answer4: null, correct: '90', type: 'input',
                explanation: ['The maximum of a box plot is the value we represent with the last bar', 'In this case, that value when rounded to the nearest ten is approximately equal to 90']},
                {question: 'What appears to be the skew of the data?', image: '../../Images/boxplot1.png', answer1: 'right', answer2: 'left', answer3: 'none', answer4: 'we cannot tell', correct: null, type: 'multiple choice',
                explanation: ['We can determine the skew in the data by making a comparison with the distance between Q1 and the median and Q3 and the median', 'Also the minimum to Q1 and the maximum to Q3', 'The side which seems most squished together (compacted) is the side which will indicate skew', 'This graph seems to be skewed to the right']},
                {question: 'What appears to be the skew of the data?', image: '../../Images/boxplot2.png', answer1: 'right', answer2: 'left', answer3: 'none', answer4: 'we cannot tell', correct: null, type: 'multiple choice',
                explanation: ['We can determine the skew in the data by making a comparison with the distance between Q1 and the median and Q3 and the median', 'Also the minimum to Q1 and the maximum to Q3', 'The side which seems most squished together (compacted) is the side which will indicate skew', 'This graph seems to be skewed to the right']},
                {question: 'What appears to be the skew of the data?', image: '../../Images/boxplot3.png', answer1: 'left', answer2: 'right', answer3: 'none', answer4: 'we cannot tell', correct: null, type: 'multiple choice',
                explanation: ['We can determine the skew in the data by making a comparison with the distance between Q1 and the median and Q3 and the median', 'Also the minimum to Q1 and the maximum to Q3', 'The side which seems most squished together (compacted) is the side which will indicate skew', 'This graph seems to be skewed to the left']},
                {question: 'What appears to be the skew of the data?', image: '../../Images/boxplot4.png', answer1: 'none', answer2: 'left', answer3: 'right', answer4: 'we cannot tell', correct: null, type: 'multiple choice',
                explanation: ['We can determine the skew in the data by making a comparison with the distance between Q1 and the median and Q3 and the median', 'Also the minimum to Q1 and the maximum to Q3', 'The side which seems most squished together (compacted) is the side which will indicate skew', 'This graph seems not to be skewed at all']},
            ];
            break;
        case "Central%20Tendency%20Quiz.html":
            var askedquestions = [
                {question: 'From this set of data - calculate the mean (where appropriate - round to 2 decimal places)', image: '../../Images/data1.png', answer1: null, answer2: null, answer3: null, answer4: null, correct: '5', type: 'input',
                explanation: ['The mean is the average value in the dataset', 'This value will be equal to (1 + 2 + 2 + 3 + 4 + 5 + 6 + 7 + 8 + 8 + 9)/n, where n = 11 as there are 11 units', 'The equals 55/11 = 5']},
                {question: 'From this set of data - calculate the mean (where appropriate - round to 2 decimal places)', image: '../../Images/data2.png', answer1: null, answer2: null, answer3: null, answer4: null, correct: '6', type: 'input',
                explanation: ['The mean is the average value in the dataset', 'This value will be equal to (9 + 4 + 3 + 8)/n where n is equal to 4', 'This is equal to 24/4 = 6']},
                {question: 'From this set of data - calculate the mean (where appropriate - round to 2 decimal places)', image: '../../Images/data3.png', answer1: null, answer2: null, answer3: null, answer4: null, correct: '4.22', type: 'input',
                explanation: ['The mean is the average value in the dataset', 'This value will be equal to (4 + 2 + 1 + 4 + 3 + 8 + 9 + 4 + 3)/n where n is equal to 9', 'This is equal to 38/9 = 4.22 when rounded to 2 decimal places']},
                {question: 'From this set of data - calculate the median', image: '../../Images/data1.png', answer1: null, answer2: null, answer3: null, answer4: null, correct: '5', type: 'input',
                explanation: ['The median is the middle value in the data set', 'When ordered our dataset is 1, 2, 2, 3, 4, 5, 6, 7, 8, 8, 9', 'Now let\'s remove number from either side of the set. We\'ll begin by removing:', '1, 9', '2, 8', '2, 8', '3, 7', '4, 6', 'This leaves us with the number 5, our median value']},
                {question: 'From this set of data - calculate the median', image: '../../Images/data2.png', answer1: null, answer2: null, answer3: null, answer4: null, correct: '6', type: 'input',
                explanation: ['The median is the middle value in the data set', 'When ordered the dataset is 3, 4, 8, 9', 'Now let\'s remove number from either side of the set. We\'ll begin by removing:', '3, 9', 'We\'re left with 2 numbers (4, 8)', 'To get the median we need to take the average, that is (4 + 8)/2 or 6']},
                {question: 'From this set of data - calculate the median', image: '../../Images/data3.png', answer1: null, answer2: null, answer3: null, answer4: null, correct: '4', type: 'input',
                explanation: ['The median is the middle value in the data set', 'When ordered the dataset is 1, 2, 3, 3, 4, 4, 4, 8, 9', 'Now let\'s remove number from either side of the set. We\'ll begin by removing:', '1, 9', '2, 8', '3, 4', '3, 4', 'This leaves us with only one number, our median, 4']},
                {question: 'From this set of data - calculate the mode (where there is more than one mode express your answer as number1,number2)', image: '../../Images/data1.png', answer1: null, answer2: null, answer3: null, answer4: null, correct: '2,8', type: 'input',
                explanation: ['The mode is the most frequent value in the data set', 'A quick count of the times a number appears in our dataset shows the numbers 2 and 8.', 'We have two modes, 2, 8']},
                {question: 'From this set of data - calculate the mode (where there is more than one mode express your answer as number1,number2)', image: '../../Images/data2.png', answer1: null, answer2: null, answer3: null, answer4: null, correct: '3,4,8,9', type: 'input',
                explanation: ['The mode is the most frequent value in the data set', 'A quick count of the times a number appears in our dataset shows that all numbers occur equally often', 'Therefore our mode is equal the dataset: 3, 4, 8, 9']},
                {question: 'From this set of data - calculate the mode (where there is more than one mode express your answer as number1,number2)', image: '../../Images/data3.png', answer1: null, answer2: null, answer3: null, answer4: null, correct: '4', type: 'input',
                explanation: ['The mode is the most frequent value in the data set', 'A quick count of the times a number appears in our dataset shows the number 4 occurs most often', 'Our mode is 4']},
                {question: 'From this set of data - calculate the range (where appropriate - round to 2 decimal places)', image: '../../Images/data1.png', answer1: null, answer2: null, answer3: null, answer4: null, correct: '8', type: 'input',
                explanation: ['The range is the maximum value minus the minimum value in the data set', 'The maximum value in our dataset is 9, the minimum is 1', 'Our range is 9 minus 1, equal to 8']},
                {question: 'From this set of data - calculate the range (where appropriate - round to 2 decimal places)', image: '../../Images/data2.png', answer1: null, answer2: null, answer3: null, answer4: null, correct: '6', type: 'input',
                explanation: ['The range is the maximum value minus the minimum value in the data set', 'The maximum value in our dataset is 9, the minimum is 3', 'Our range is 9 minus 3, equal to 6']},
                {question: 'From this set of data - calculate the interquartile range (where appropriate - round to 2 decimal places)', image: '../../Images/data1.png', answer1: null, answer2: null, answer3: null, answer4: null, correct: '6', type: 'input',
                explanation: ['The range is the Q3 value minus the Q1 value in the data set', 'First we need to organise the dataset, this would be 1, 2, 2, 3, 4, 5, 6, 7, 8, 8, 9', 'Now we need to calculate the median.', 'Now let\'s remove number from either side of the set. We\'ll begin by removing:', '1, 9', '2, 8', '2, 8', '3, 7', '4, 6', 'This leaves us with the number 5, our median value', 'We then remove this value from the dataset, and create two new datasets 1, 2, 2, 3, 4 and 6, 7, 8, 8, 9', 'Now we need the middle values in these sets', 'From the first set we\'d removed (1, 4), (2, 3). This gives us our Q1 value of 2', 'From the second set we\'d removed (6, 9), (7, 8). This gives us our Q3 value of 8', 'Now the interquartile range is Q3 minus Q1 or 8 - 2', 'Our interquartile range is equal to 6']},
                {question: 'From this set of data - calculate the interquartile range (where appropriate - round to 2 decimal places)', image: '../../Images/data2.png', answer1: null, answer2: null, answer3: null, answer4: null, correct: '5', type: 'input',
                explanation: ['The range is the Q3 value minus the Q1 value in the data set', 'First we need to organise the dataset, this would be 3, 4, 8, 9', 'Now we need to calculate the median.', 'Now let\'s remove number from either side of the set. We\'ll begin by removing:', '3, 9', '3, 9', 'We\'re left with 2 numbers (4, 8)', 'To get the median we need to take the average, that is (4 + 8)/2 or 6', 'We cannot remove our median from the dataset because it was created, so we split our dataset in two new datasets 3, 4 and 8, 9', 'Now we need the middle values in these sets', 'From the first set we create an average because we have to create a middle. This gives us our Q1 value of 3.5', 'From the second set we create a mean of (8 + 9)/2. This gives us our Q3 value of 8.5', 'Now the interquartile range is Q3 minus Q1 or 8.5 - 3.5', 'Our interquartile range is equal to 5']},
                {question: 'From this set of data - calculate the standard devitation (where appropriate - round to 2 decimal places)', image: '../../Images/data1.png', answer1: null, answer2: null, answer3: null, answer4: null, correct: '2.34', type: 'input',
                explanation: ['The standard deviation is the spread the we see in the data', 'It\'s calculted as the (square root((sum of ((x index values - the mean) sqaured)) divided by n))', 'First we need to calculate the mean. This is (1 + 2 + 2 + 3 + 4 + 5 + 6 + 7 + 8 + 8 + 9)/11, or 5', 'Let\'s begin by summing our x index - mean squared values. First', '(1 - 5)^2 = -4^2 = 16', '(2 - 5)^2 = -3^2 = 9', '(2 - 5)^2 = -3^2 = 9', '(3 - 5)^2 = -2^2 = 4', '(4 - 5)^2 = -1^2 = 1', '(5 - 5)^2 = 0^2 = 0', '(6 - 5)^2 = 1^2 = 1', '(7 - 5)^2 = 2^2 = 4', '(8 - 5)^2 = 3^2 = 9', '(8 - 5)^2 = 3^2 = 9', '(9 - 5)^2 = 4^2 = 16', 'This equals 16 + 9 + 4 + 1 + 0 + 1 + 4 + 9 + 16 or 60', 'Next we need to divide this by our n of 11. This gives us 60/11', 'Finally we need to square root this value for our standard deviation', 'This equals 2.34 when rounded to two decimal places']},
                {question: 'From this set of data - calculate the standard devitation (where appropriate - round to 2 decimal places)', image: '../../Images/data2.png', answer1: null, answer2: null, answer3: null, answer4: null, correct: '2.55', type: 'input',
                explanation: ['The standard deviation is the spread the we see in the data', 'It\'s calculted as the (square root((sum of ((x index values - the mean) sqaured)) divided by n))', 'First we need to calculate the mean. This is (3 + 4 + 8 + 9)/4, or 6', 'Let\'s begin by summing our x index - mean squared values. First', '(3 - 6)^2 = -3^2 = 9', '(4 - 6)^2 = -2^2 = 4', '(8 - 6)^2 = 2^2 = 4', '(9 - 6)^2 = 3^2 = 9', 'This equals 9 + 4 + 4 + 9 or 26', 'Next we need to divide this by our n of 4. This gives us 26/4', 'Finally we need to square root this value for our standard deviation', 'This equals 2.55 when rounded to two decimal places']},
                {question: 'From this set of data - calculate the standard devitation (where appropriate - round to 2 decimal places)', image: '../../Images/data3.png', answer1: null, answer2: null, answer3: null, answer4: null, correct: '2.48', type: 'input',
                explanation: ['The standard deviation is the spread the we see in the data', 'It\'s calculted as the (square root((sum of ((x index values - the mean) sqaured)) divided by n))', 'First we need to calculate the mean. This is (1 + 2 + 3 + 3 + 4 + 4 + 4 + 8 + 9)/9, or 4.222222222', 'Let\'s begin by summing our x index - mean squared values. First', '(1 - 4.222222222)^2 = -3.222222222^2 = 10.38271605', '(2 - 4.222222222)^2 = -2.222222222^2 = 4.938271605', '(3 - 4.222222222)^2 = -1.222222222^2 = 1.493827155', '(3 - 4.222222222)^2 = -1.222222222^2 = 1.493827155', '(4 - 4.222222222)^2 = -0.222222222^2 = 0.04938271595', '(4 - 4.222222222)^2 = 0.222222222^2 = 0.04938271595', '(4 - 4.222222222)^2 = 1^2 = 0.04938271595', '(8 - 4.222222222)^2 = 3.777777777^2 = 14.27160494', '(9 - 4.222222222)^2 = 4.777777777^2 = 22.82716049', 'This equals 55.55563554', 'Next we need to divide this by our n of 9. This gives us 55.55563554/9', 'Finally we need to square root this value for our standard deviation', 'This equals 2.48 when rounded to two decimal places']},
                {question: 'If the standard deviation is 4 what is the variance? (where appropriate - round to 2 decimal places)', image: null, answer1: null, answer2: null, answer3: null, answer4: null, correct: '16', type: 'input',
                explanation: ['We need to remember that the variance is equal to the standard deviation squared', 'As our standard deviation is 4, the squared value is equal to 16']},
                {question: 'If the standard deviation is 7.5 what is the variance? (where appropriate - round to 2 decimal places)', image: null, answer1: null, answer2: null, answer3: null, answer4: null, correct: '56.25', type: 'input',
                explanation: ['We need to remember that the variance is equal to the standard deviation squared', 'As our standard deviation is 7.5, the squared value is equal to 56.25']},
                {question: 'If the standard deviation is 4 and the mean is 14, what is the coefficient of variation? (where appropriate - round to 2 decimal places)', image: null, answer1: null, answer2: null, answer3: null, answer4: null, correct: '0.29', type: 'input',
                explanation: ['The coefficient of variation is standard deviation divided by the mean', 'This is equal to 4/14 or 0.29 when rounded to 2 decimal places']},
                {question: 'If the standard deviation is 7.5 and the mean is 22.5, what is the coefficient of variation? (where appropriate - round to 2 decimal places)', image: null, answer1: null, answer2: null, answer3: null, answer4: null, correct: '0.33', type: 'input',
                explanation: ['The coefficient of variation is standard deviation divided by the mean', 'This is equal to 7.5/22.5 or 0.33 when rounded to 2 decimal places']},
                {question: 'Using the empirical rule, what proportion of the dataset can be represented between -3(sigma) and 2(sigma)', image: '../../Images/empirical.png', answer1: '0.9735', answer2: '0.475', answer3: '0.235', answer4: '0.815', correct: null, type: 'multiple choice',
                explanation: ['The emirical rule as strict values.', 'Between the mean and one sigma in either direction, the proportion of the dataset represented is 34%', 'Between 2 sigma and one sigma, or -2 sigma and one sigma, the proportion of a dataset represented is 13.5%', 'Between 3 sigma and 2 sigma, or -3 sigma and -2 sigma, the proportion of a dataset represented is 2.35%', 'Our value of interest -3 sigma to 2 sigma therefore equals 2.35% + 13.5% + 34% + 34% + 13.5%', 'This totals to 97.35%, which we express as a proportion 0.9735']},
                {question: 'Using the empirical rule, what proportion of the dataset can be represented between 0(sigma) and 2(sigma) ', image: '../../Images/empirical.png', answer1: '0.475', answer2: '0.9735', answer3: '0.235', answer4: '0.815', correct: null, type: 'multiple choice',
                explanation: ['The emirical rule as strict values.', 'Between the mean and one sigma in either direction, the proportion of the dataset represented is 34%', 'Between 2 sigma and one sigma, or -2 sigma and one sigma, the proportion of a dataset represented is 13.5%', 'Between 3 sigma and 2 sigma, or -3 sigma and -2 sigma, the proportion of a dataset represented is 2.35%', 'Our value of interest 0 sigma to 2 sigma therefore equals 34% + 13.5%', 'This totals to 47.5%, which we express as a proportion 0.475']},
                {question: 'Using the empirical rule, what proportion of the dataset can be represented between -3(sigma) and -2(sigma) ', image: '../../Images/empirical.png', answer1: '0.235', answer2: '0.9735', answer3: '0.475', answer4: '0.815', correct: null, type: 'multiple choice',
                explanation: ['The emirical rule as strict values.', 'Between the mean and one sigma in either direction, the proportion of the dataset represented is 34%', 'Between 2 sigma and one sigma, or -2 sigma and one sigma, the proportion of a dataset represented is 13.5%', 'Between 3 sigma and 2 sigma, or -3 sigma and -2 sigma, the proportion of a dataset represented is 2.35%', 'Our value of interest -3 sigma to -2 sigma therefore equals 2.35%, which we express as a proportion 0.0235']},
                {question: 'Using the empirical rule, what proportion of the dataset can be represented between -1(sigma) and 2(sigma) ', image: '../../Images/empirical.png', answer1: '0.815', answer2: '0.9735', answer3: '0.475', answer4: '0.235', correct: null, type: 'multiple choice',
                explanation: ['The emirical rule as strict values.', 'Between the mean and one sigma in either direction, the proportion of the dataset represented is 34%', 'Between 2 sigma and one sigma, or -2 sigma and one sigma, the proportion of a dataset represented is 13.5%', 'Between 3 sigma and 2 sigma, or -3 sigma and -2 sigma, the proportion of a dataset represented is 2.35%', 'Our value of interest -1 sigma to 2 sigma therefore equals 34% + 34% + 13.5%', 'This totals to 81.5%, which we express as a proportion 0.815']},
            ];
            break;
    }   

    do
    {
        var selected = Math.floor(Math.random() * askedquestions.length);
    } while (selected == questionIndex);

    questionIndex = selected;

    return askedquestions[selected];
}

function prepareQuestion() {
    //call question
    var info = questionset();

    if (info.type === 'multiple choice')
    {
        function multipleChoice() {
            //create question elements
            var body = document.getElementsByTagName('body')[0];
            var quizspace = document.createElement('div');
            var question = document.createElement('p');
            var image = document.createElement('img');
            var table = document.createElement('table');
            var tbody = document.createElement('tbody');
            var next = document.createElement('a');
            var explain = document.createElement('a');
            var explaindiv = document.createElement('div');
            var backtoquestion = document.createElement('button');

            //create the space for the quiz to be in
            quizspace.setAttribute('id', 'quiz_Space');   

            //create the explanation
            explaindiv.setAttribute('id', 'explain_Space');
            for (i = 0; i < info.explanation.length; i++)
            {
                explaindiv.appendChild(document.createTextNode(info.explanation[i]));
                explaindiv.appendChild(document.createElement('br'));
            }            
            explaindiv.appendChild(document.createTextNode(info.explanation));
            explaindiv.appendChild(document.createElement('br'));
            explaindiv.appendChild(document.createElement('br'));
            backtoquestion.appendChild(document.createTextNode('Ok, I get it, lets go back'));
            backtoquestion.style.width = '200px';
            backtoquestion.onclick = function() {
                explaindiv.style.display = 'none';
            }
            explaindiv.appendChild(backtoquestion);
            explaindiv.style.display = 'none';

            //input question
            question.appendChild(document.createTextNode(info.question));                     
            quizspace.appendChild(question);

            //input image
            if (info.image !== null)
            {
                image.src = info.image;
                image.setAttribute('id', 'multi');            
                quizspace.appendChild(image);
            }

            //create table rows, table data, and buttons
            var tr1 = document.createElement('tr');
            var tr2 = document.createElement('tr');
            var tr3 = document.createElement('tr');
            var tr4 = document.createElement('tr');
            var td1 = document.createElement('td');
            var td2 = document.createElement('td');
            var td3 = document.createElement('td');
            var td4 = document.createElement('td');
            var td5 = document.createElement('td');
            var td6 = document.createElement('td');
            var td7 = document.createElement('td');
            var td8 = document.createElement('td');
            var btn1 = document.createElement('button');
            var btn2 = document.createElement('button');
            var btn3 = document.createElement('button');
            var btn4 = document.createElement('button');

            //puts questions and buttons into table data            
            btn1.onclick = function() {
                td1.style.backgroundColor = 'green';
            }
            btn2.onclick = function() {
                td3.style.backgroundColor = 'red';
            }   
            btn3.onclick = function() {
                td5.style.backgroundColor = 'red';
            } 
            btn4.onclick = function() {
                td7.style.backgroundColor = 'red';
            } 
            td1.appendChild(document.createTextNode(info.answer1));
            td2.appendChild(btn1);
            td3.appendChild(document.createTextNode(info.answer2));
            td4.appendChild(btn2);
            td5.appendChild(document.createTextNode(info.answer3));
            td6.appendChild(btn3);
            td7.appendChild(document.createTextNode(info.answer4));
            td8.appendChild(btn4);

            //add information to rows
            tr1.appendChild(td1);
            tr1.appendChild(td2);                   
            tr2.appendChild(td3);
            tr2.appendChild(td4);
            tr3.appendChild(td5);
            tr3.appendChild(td6);
            tr4.appendChild(td7);
            tr4.appendChild(td8);

            //random sorting
            var num1 = Math.ceil(Math.random() * 4);
            var num2;
            var num3;
            var num4;
            do
            {
                num2 = Math.ceil(Math.random() * 4);
            } while (num1 === num2)
            do
            {
                num3 = Math.ceil(Math.random() * 4);
            } while (num3 === num2 || num3 === num1)
            do
            {
                num4 = Math.ceil(Math.random() * 4);
            } while (num4 === num3 || num4 === num2 || num4 === num1)
            
            //put information into table
            switch(num1)
            {
                case 1:
                    tbody.appendChild(tr1);
                    break;
                case 2:
                    tbody.appendChild(tr2);
                    break;
                case 3:
                    tbody.appendChild(tr3);
                    break;
                case 4:
                    tbody.appendChild(tr4);
                    break;
            }
            switch(num2)
            {
                case 1:
                    tbody.appendChild(tr1);
                    break;
                case 2:
                    tbody.appendChild(tr2);
                    break;
                case 3:
                    tbody.appendChild(tr3);
                    break;
                case 4:
                    tbody.appendChild(tr4);
                    break;
            }
            switch(num3)
            {
                case 1:
                    tbody.appendChild(tr1);
                    break;
                case 2:
                    tbody.appendChild(tr2);
                    break;
                case 3:
                    tbody.appendChild(tr3);
                    break;
                case 4:
                    tbody.appendChild(tr4);
                    break;
            }
            switch(num4)
            {
                case 1:
                    tbody.appendChild(tr1);
                    break;
                case 2:
                    tbody.appendChild(tr2);
                    break;
                case 3:
                    tbody.appendChild(tr3);
                    break;
                case 4:
                    tbody.appendChild(tr4);
                    break;
            }
            table.appendChild(tbody);

            //another question link
            next.appendChild(document.createTextNode('Another Question?'));
            next.href = '#';
            next.onclick = function() {
                prepareQuestion();
            }

            //explain the reasoning for the questions
            explain.appendChild(document.createTextNode('I need an explanation'));
            explain.href = '#';
            explain.onclick = function() {
                explaindiv.style.display = 'block';
            }

            //placement of explanation and next
            explain.style.position = 'absolute';
            explain.style.top = '55vh';
            next.style.position = 'absolute';
            next.style.top = '55vh';
            next.style.left = '12vw';

            //append information to the screen
            quizspace.appendChild(table);
            quizspace.appendChild(explain);
            quizspace.appendChild(next);
            quizspace.appendChild(explaindiv);
            body.appendChild(quizspace);
        }

        window.onload = multipleChoice();
    }
    else if (info.type === 'input')
    {
        function inputQuestion() {
            //create question elements
            var body = document.getElementsByTagName('body')[0];
            var quizspace = document.createElement('div');
            var question = document.createElement('p');
            var input = document.createElement('input');
            var submit = document.createElement('button');
            var image = document.createElement('img');
            var next = document.createElement('a');
            var wrong = document.createElement('p');            
            var right = document.createElement('p');
            var explain = document.createElement('a');
            var explaindiv = document.createElement('div');
            var backtoquestion = document.createElement('button');

            //create the space for the quiz to be in
            quizspace.setAttribute('id', 'quiz_Space');

            //create the space for the explanation
            explaindiv.setAttribute('id', 'explain_Space');
            for (i = 0; i < info.explanation.length; i++)
            {
                explaindiv.appendChild(document.createTextNode(info.explanation[i]));
                explaindiv.appendChild(document.createElement('br'));
            }            
            explaindiv.appendChild(document.createElement('br'));
            explaindiv.appendChild(document.createElement('br'));
            backtoquestion.appendChild(document.createTextNode('Ok, I get it, lets go back'));
            backtoquestion.style.width = '200px';
            backtoquestion.onclick = function() {
                explaindiv.style.display = 'none';
            }
            explaindiv.appendChild(backtoquestion);
            explaindiv.style.display = 'none';

            //input question
            question.appendChild(document.createTextNode(info.question));
            quizspace.appendChild(question);

            //input image
            if (info.image !== null)
            {
                image.src = info.image;
                image.setAttribute('id', 'input');
                quizspace.appendChild(image);
            }            

            //create valid answer checks
            wrong.appendChild(document.createTextNode('Incorrect'));
            wrong.style.backgroundColor = 'red';
            right.appendChild(document.createTextNode('Correct'));
            right.style.backgroundColor = 'green';

            //create submit element
            submit.appendChild(document.createTextNode('submit answer'));
            submit.onclick = function() {
                var checknum;
                try
                {
                    checknum == parseFloat(input.value);
                }
                finally{}

                if (input.value.toLowerCase().split(' ').join('') == info.correct || checknum == info.correct)
                {
                    wrong.style.display = 'none';
                    right.style.display = 'block';
                }
                else
                {
                    right.style.display = 'none';
                    wrong.style.display = 'block';
                }
            }
            submit.setAttribute('id', 'submitButton');

            //another question link
            next.appendChild(document.createTextNode('Another Question?'));
            next.href = '#';
            next.onclick = function() {
                prepareQuestion();
            }

            //explain the reasoning for the questions
            explain.appendChild(document.createTextNode('I need an explanation'));
            explain.href = '#';
            explain.onclick = function() {
                explaindiv.style.display = 'block';
            }

            //placement of explanation and next
            explain.style.position = 'absolute';
            explain.style.top = '55vh';
            explain.style.left = '1vw';
            next.style.position = 'absolute';
            next.style.top = '55vh';
            next.style.left = '12vw';

            //append information to the screen
            quizspace.appendChild(document.createElement('br'));
            quizspace.appendChild(input);
            quizspace.appendChild(submit);
            quizspace.appendChild(explain);
            quizspace.appendChild(next);
            quizspace.appendChild(wrong);
            quizspace.appendChild(right);
            quizspace.appendChild(explaindiv);  
            body.appendChild(quizspace);    

            //hide answers
            wrong.style.display = 'none';
            right.style.display = 'none';        
        }

        window.onload = inputQuestion();
    }
}
